//2. Haz una clase llamada Password que siga las siguientes condiciones:

//� Que tenga los atributos longitud y contrase�a . Por defecto, la longitud ser� de 8. 
//� Los constructores ser�n los siguiente:

//- Un constructor por defecto.

 //- Un constructor con la longitud que nosotros le pasemos. Generara una contrase�a aleatoria con esa longitud.


package Password;

public class Password {

	   private final static int LONG_DEF=8;
	  
	    //Longitud de la contrase�a
	    
	    private int longitud;
	   
	    private String contrase�a;
	  	   
	    //Devuelve la longitud de la contrase�a
	     
	    public int getLongitud() {
	        return longitud;
	    }
	  
	    //Modifica la longitud de la contrase�a
	     
	    public void setLongitud(int longitud) {
	        this.longitud = longitud;
	    }
	  
	    //Devuelve la contrase�a
	        
	    public String getContrase�a() {
	        return contrase�a;
	    }
	  
	    //Genera una contrase�a al azar con la longitud definida
	     
	    public String generaPassword (){
	        String password="";
	        for (int i=0;i<longitud;i++){
	            //Generamos un numero aleatorio, segun este elige si a�adir una minuscula, mayuscula o numero
	            int eleccion=((int)Math.floor(Math.random()*3+1));
	  
	            if (eleccion==1){
	                char minusculas=(char)((int)Math.floor(Math.random()*(123-97)+97));
	                password+=minusculas;
	            }else{
	                if(eleccion==2){
	                    char mayusculas=(char)((int)Math.floor(Math.random()*(91-65)+65));
	                    password+=mayusculas;
	                }else{
	                    char numeros=(char)((int)Math.floor(Math.random()*(58-48)+48));
	                    password+=numeros;
	                }
	            }
	        }
	        return password;
	    }
	  
	    //Comprueba la fortaleza de la contrase�a
	     
	    public boolean esFuerte(){
	        int cuentanumeros=0;
	        int cuentaminusculas=0;
	        int cuentamayusculas=0;
	        //Vamos caracter a caracter y comprobamos que tipo de caracter es
	        for (int i=0;i<contrase�a.length();i++){
	                if (contrase�a.charAt(i)>=97 && contrase�a.charAt(i)<=122){
	                    cuentaminusculas+=1;
	                }else{
	                    if (contrase�a.charAt(i)>=65 && contrase�a.charAt(i)<=90){
	                        cuentamayusculas+=1;
	                }else{
	                    cuentanumeros+=1;
	                    }
	                }
	            }
	            //Si la constrase�a tiene mas de 5 numeros, mas de 1 minuscula y mas de 2 mayusculas
	            if (cuentanumeros>=5 && cuentaminusculas>=1 && cuentamayusculas>=2){
	            return true;
	        }else{
	            return false;
	        }
	    }
	  
	     //Crea una contrase�a al azar
	    
	    public Password (){
	        this(LONG_DEF);
	    }
	  
	    //La contrase�a sera la pasada por parametro
	    
	    public Password (int longitud){
	        this.longitud=longitud;
	        contrase�a=generaPassword();
	   }
	    
}
