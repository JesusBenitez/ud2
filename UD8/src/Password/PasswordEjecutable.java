package Password;

import javax.swing.JOptionPane;

public class PasswordEjecutable {

	public static void main(String[] args) {
		
	//Introducimos tamaño del array y longitud del password
        String texto=JOptionPane.showInputDialog("Introduce un tamaño para el array");
        int tamanio=Integer.parseInt(texto);
  
        texto=JOptionPane.showInputDialog("Introduce longitud del password");
        int longitud=Integer.parseInt(texto);
  
        //Creamos los arrays
        Password listaPassword[]=new Password[tamanio];
        boolean fortalezaPassword[]=new boolean[tamanio];
  
        //Mostramos la contraseña y su fortaleza.
        for(int i=0;i<listaPassword.length;i++){
            
        listaPassword[i]=new Password(longitud);
        fortalezaPassword[i]=listaPassword[i].esFuerte();
        System.out.println(listaPassword[i].getContrasea()+" "+fortalezaPassword[i]);
        }

	}

}
