import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Scanner;
public class Ejercicio1 {

	public static void main(String[] args) {
		// Crea una aplicaci�n que calcule la nota media de los alumnos pertenecientes al curso de programaci�n. 
		//Una vez calculada la nota media se guardar� esta informaci�n en un diccionario de datos que almacene la nota media de cada alumno. 
		//Todos estos datos se han de proporcionar por pantalla.

		//Declaramos variables
		
		int alumnos=2;
		double modul1=0;
		double modul2=0;				
		double media[]=new double [alumnos];
						
		media= rellenarArrayMedias(media, modul1, modul2);
				
		//Definir HASHTABLE
						
		//Lo primero ser� definir una variable contenedor para instanciar la clase Hashtable Java:
		
		Hashtable<String,Double> alumno=new Hashtable<String,Double>();
		
		//El primer elemento ser� la clave y el segundo ser� el valor a almacenar.
				
		alumno.put("44455566A", media[0]);
		alumno.put("56789245F", media[1]);
					
		//Mostrar contenido HASTABLE 
		
		//Mediante un Enumeration vamos a recorrer el contenido de nuestra Hashtable Java
				
		Enumeration<Double> enumeration = alumno.elements();
		Enumeration<String> llaves = alumno.keys();
				while(enumeration.hasMoreElements()&& llaves.hasMoreElements()) {
					//Si queremos saber cuales son las claves de la Hashtable Java usamos el m�todo .keys().
					System.out.println(""+"Valores: "+llaves.nextElement()+" " + enumeration.nextElement());
				}
				
			}
			
		public static double[] rellenarArrayMedias(double medias[], double modul1, double modul2) {
		
			Scanner scan = new Scanner(System.in);
				for (int i = 0; i < medias.length; i++) {
					System.out.println("Nota del m�dulo 1 del alumno "+i);
					modul1 = scan.nextDouble();
					
					System.out.println("Nota del m�dulo 2 del alumno "+i);
					modul2 = scan.nextDouble();
												
					double media = calculoMedia(modul1, modul2);
					
					medias[i] = media;
					
				}
				return medias;
			}
			
			public static double calculoMedia(double modul1, double modul2) {
				
			double media=0;	
			media = (modul1 + modul2)/2;
		
			return media;
	      }
			
			//Mostrar array
			public static void mostrarArray(double num[]) {
				for (int i = 0; i < num.length; i++) {
					System.out.println(num[i]);
	}

}
			
}
		
