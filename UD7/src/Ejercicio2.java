import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
public class Ejercicio2 {

	public static void main(String[] args) {
		// Crea una aplicaci�n que gestione el flujo de ventas de una caja de supermercado. 
		//El programa guardar� la cantidades del carrito de compra dentro de una lista. 

        //Mostrar� por pantalla la siguiente informaci�n:

        //� IVA aplicado (21% o 4%) 
        // � precio total bruto y precio mas IVA. 
        //� N�mero de art�culos comprados. 
        // � Cantidad pagada. 
        // � Cambio a devolver al cliente.

       Scanner scan = new Scanner(System.in);
		
	   ArrayList<Double> carro = new ArrayList<>();
		
		int numproductos=0;
		int iva; //Aqui deberiamos a�adir los valores 21 o 4 para definir-lo y hacer una operaci�n como esta var cantidadIVA = precioNeto * IVA;
		double pagado=0;
		double cambio=0;
		double total=0;
		boolean salir=false;
		
		do {
			System.out.println("Precio del producto?");
			double precio = scan.nextDouble();
			
			carro.add(precio);
						
			System.out.println("Quiere comprar alg�n producto m�s. (Si o No)");
			String continuar = scan.next();
			
			if (continuar.equals("si")) {
				salir = true;
			}else {
				salir = false;
				System.out.println("Cantidad pagada?");
				pagado = scan.nextDouble();
			}
			
			cambio = pagado - total;
			total = total + precio;
		} while (salir == true);
		
		Iterator<Double> it = carro.iterator();
		double numero;
		while(it.hasNext()) {
			numero = it.next();
			System.out.println(numero);
			numproductos++;
		}
		
		System.out.println("Numero total de productos: "+numproductos);
		System.out.println("Cambio a devolver al cliente: "+cambio);
	}

}
