import javax.swing.JOptionPane;
public class Ejercicio8 {

	public static void main(String[] args) {
		// Crea un array de 10 posiciones de n�meros con valores pedidos por teclado. 
		//Muestra por consola el indice y el valor al que corresponde. Haz dos m�todos, uno para rellenar valores y otro para mostrar .

        final int posiciones=10;
 
        int num[]=new int[posiciones];
 
        rellenarArray(num);
 
        mostrarArray(num);
    }
 
    public static void rellenarArray(int lista[]){
        for(int i=0;i<lista.length;i++){
            String texto=JOptionPane.showInputDialog("Introduce un n�mero");
            lista[i]=Integer.parseInt(texto);
        }
    }
 
    public static void mostrarArray(int lista[]){
        for(int i=0;i<lista.length;i++){
            System.out.println("En el indice "+i+" esta el valor "+lista[i]);
        }

	}

}
