import javax.swing.JOptionPane;
public class Ejercicio4 {

	public static void main(String[] args) {
		// Crea una aplicaci�n que nos calcule el factorial de un n�mero pedido por teclado, 
		//lo realizara mediante un m�todo al que le pasamos el n�mero como par�metro. 
		//Para calcular el factorial, se multiplica los n�meros anteriores hasta llegar a uno. 
		//Por ejemplo, si introducimos un 5, realizara esta operaci�n 5*4*3*2*1=120.
	
		String texto=JOptionPane.showInputDialog("Introduce un numero");
        int numero=Integer.parseInt(texto);
        System.out.println("El factorial de "+numero+ " es " +factorial(numero));
    }
        public static int factorial (int numero){
        int res=numero;
        
        for(int cont=(numero-1);cont>0;cont--){
            res=res*cont;
        }
        return res;

	}

}
