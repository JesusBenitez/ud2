import javax.swing.JOptionPane;
public class Ejercicio7 {

	public static void main(String[] args) {
		//Crea un aplicaci�n que nos convierta una cantidad de euros introducida por teclado a otra moneda, 
		//estas pueden ser a dolares, yenes o libras. 
		//El m�todo tendr� como par�metros, la cantidad de euros y la moneda a pasar que sera una cadena, este no devolver� ning�n valor , 
		//mostrara un mensaje indicando el cambio (void).
        //El cambio de divisas son: 0.86 libras es un 1 � 1.28611 $ es un 1 � 129.852 yenes es un 1 �

		String texto=JOptionPane.showInputDialog("Escribe una cantidad en euros");
        double cantidad=Double.parseDouble(texto);
        String moneda=JOptionPane.showInputDialog("Escribe la moneda a la que quieres convertir");
        conversor(cantidad, moneda);
    }
   public static void conversor (double cantidad, String moneda){
        double res=0;
 
        //Si no es un nombre de moneda correcto
        boolean correcto = true;
 
        //Calculamos el valor segun la moneda
        
        switch (moneda){
        case "libras":
            res=cantidad*0.86;
            break;
        case "dolares":
            res=cantidad*1.29;
            break;
        case "yenes":
            res=cantidad*129.852;
            break;
        default:
            System.out.println("No has introducido una moneda correcta");
            correcto=false;
        }
 
        //Si los datos son correctos.
        if (correcto){
            System.out.println(cantidad+ " euros en " +moneda+ " son " +res);
        }
 

	}

}
