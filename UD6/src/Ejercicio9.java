import javax.swing.JOptionPane;
public class Ejercicio9 {

	public static void main(String[] args) {
		// Crea un array de n�meros donde le indicamos por teclado el tama�o del array, 
		//rellenaremos el array con n�meros aleatorios entre 0 y 9. 
		//Al final muestra por pantalla el valor de cada posici�n y la suma de todos los valores. 
		//Tareas a realizar: Haz un m�todo para rellenar el array(que tenga como par�metros los n�meros entre los que tenga que generar) 
        //y otro para mostrar el contenido y la suma del array. 

        String texto=JOptionPane.showInputDialog("Introduce un tama�o");
        int num[]=new int[Integer.parseInt(texto)];
 
        
        rellenarNumAleatorioArray(num, 0, 9);
 
        mostrarArray(num);
    }
 
    public static void rellenarNumAleatorioArray(int lista[], int a, int b){
        for(int i=0;i<lista.length;i++){
            
     //Generamos un n�mero con los parametros dados
        	
     lista[i]=((int)Math.floor(Math.random()*(a-b)+b));
        
        }
    }
 
    public static void mostrarArray(int lista[]){
        for(int i=0;i<lista.length;i++){
            System.out.println("En el indice "+i+" esta el valor "+lista[i]);
        }
		

	}

}
