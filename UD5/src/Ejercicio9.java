
public class Ejercicio9 {

	public static void main(String[] args) {
		// Muestra los n�meros del 1 al 100 (ambos incluidos) divisibles entre 2 y 3. Utiliza el bucle que desees.
		
		int num=1;
		 
        //Definimos el bucle, incluyendo el 100
        while (num<=100){
            if (num%2==0 || num%3==0){
                System.out.println(num);
        }
       
        //Incrementamos num
        num++;
       
        }
	}

}
