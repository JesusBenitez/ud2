import java.util.Scanner;

public class Ejercicio13 {

	public static void main(String[] args) {
		// Crea una aplicaci�n llamada Calculadora Inversa, nos pedir� 2 operandos (int) y un signo aritm�tico (String), 
		//seg�n este �ltimo se realizar� la operaci�n correspondiente. Al final mostrar� el resultado en un cuadro de di�logo.

         //Los signos aritm�ticos disponibles son:
         //+: suma los dos operandos. 
         //-: resta los operandos. 
         //*: multiplica los operandos.
         //: divide los operandos, este debe dar un resultado con decimales (double) 
         //^: 1� operando como base y 2� como exponente. 
         //%: m�dulo, resto de la divisi�n entre operando1 y operando 2.


		Scanner sc = new Scanner(System.in);
       
        double operador1;
        double operador2;
        double resultado=0;
  
        //Nos pide los operandos y el signo de operacion
        System.out.println("Escribe el operando 1");
        operador1=sc.nextDouble();
         
        System.out.println("Escribe el codigo de operacion");
        String operacion = sc.next();
         
        System.out.println("Escribe el operando 2");
        operador2=sc.nextDouble();
  
        //segun el codigo de operacion, haremos una u otra accion
        switch (operacion){
            case "+":
                resultado=operador1+operador2;
                break;
            case "-":
                resultado=operador1-operador2;
                break;
            case "*":
                resultado=operador1*operador2;
                break;
            case "/":
                resultado=operador1/operador2;
                break;
            case "^":
                resultado=(int)Math.pow(operador1, operador2);
                break;
            case "%":
                resultado=operador1%operador2;
                break;
        }
  
        System.out.println(operador1+" "+operacion+" "+operador2+" = "+resultado);
	}

}
