import java.util.Scanner;

public class Ejercicio10 {

	public static void main(String[] args) {
		// Realiza una aplicaci�n que nos pida un n�mero de ventas a introducir, 
		//despu�s nos pedir� tantas ventas por teclado como n�mero de ventas se hayan indicado. 
		//Al final mostrar� la suma de todas las ventas. Piensa que es lo que se repite y lo que no.

		Scanner sc = new Scanner(System.in);
        System.out.println("Introduce el n�mero de ventas");
        int numVentas=sc.nextInt();
  
        //Declaramos una variable para sumar las ventas,
         
        int sumaVentas=0;
        for (int i=0;i<numVentas;i++){
            
        //Indicamos el precio de la venta
         
        System.out.println("Introduce el precio de la venta "+(i+1));
        int venta=sc.nextInt();
  
         //Acumulamos el valor de la venta
         sumaVentas=sumaVentas+venta;
        
        }
  
        System.out.println(sumaVentas);
	}

}
