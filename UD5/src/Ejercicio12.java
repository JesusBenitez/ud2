import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {
		// 12) Escribe una aplicaci�n con un String que contenga una contrase�a cualquiera.
		//Despu�s se te pedir� que introduzcas la contrase�a, con 3 intentos. 
		//Cuando aciertes ya no pedir� m�s la contrase�a y mostrar� un mensaje diciendo �Enhorabuena�. 
		//Piensa bien en la condici�n de salida (3 intentos y si acierta sale, aunque le queden intentos).
		
		Scanner sc = new Scanner(System.in);
        String contrase�a="Jesus";
  
        final int INTENTOS = 3;
         
        //boolean nos controlara que en caso de que acierte la condicion cambie
        boolean acierto=false;
  
        String password;
        for (int i=0;i<INTENTOS && !acierto;i++){
            System.out.println("Introduce una contrase�a");
            password = sc.next();
  
            //Comprobamos si coincide con el metodo equals que
            //Compara la cadena de texto contra un objeto. Devolver� true si las cadenas comparadas son iguales. 
            //En caso contrario devolver� false.
            
            if (password.equals(contrase�a)){
                System.out.println("Enhorabuena, acertaste");
                acierto=true;
            }
        }

	}

}
