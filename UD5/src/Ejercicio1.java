
public class Ejercicio1 {

	public static void main(String[] args) {
		//Declara 2 variables num�ricas (con el valor que desees), he indica cual es mayor de los dos. 
		//Si son iguales indicarlo tambi�n. Ves cambiando los valores para comprobar que funciona.

		//Declaramos las variables
        int num1=40;
        int num2=20;
 
        //Hacemos la comprobaci�n
        if (num1>=num2){
        	
            if(num1==num2){
                System.out.println("Los numeros "+num1+" y "+num2+" son iguales");
            }else{
                System.out.println("El n�mero "+num1+" es mayor que el n�mero "+num2);
            }
        }else{
            System.out.println("El n�mero "+num2+" es mayor que el n�mero "+num1);
        }
	}

}
