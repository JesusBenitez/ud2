import javax.swing.JOptionPane;
public class Ejercicio3 {

	public static void main(String[] args) {
		// Modifica la aplicación anterior, para que nos pida el nombre que queremos introducir (recuerda usar JOptionPane).
		
		//Aparece un cuadro de dialogo
        String nombre=JOptionPane.showInputDialog("Introduce tu nombre");
 
        System.out.println("Bienvenido "+nombre);

	}

}
