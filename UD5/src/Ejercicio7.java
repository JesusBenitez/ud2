
public class Ejercicio7 {

	public static void main(String[] args) {
		// Muestra los n�meros del 1 al 100 (ambos incluidos). Usa un bucle while.

		int num=1;
		 
        //Definimos el bucle, incluyendo el numero 100
        while (num<=100){
        
        System.out.println(num);
        //Incrementamos num
        num++;
        
        }
	}

}
